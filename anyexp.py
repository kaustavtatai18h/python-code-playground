# Enter your code here. Read input from STDIN. Print output to STDOUT

num   = input();
exp   = input();
exp   = exp.split(' ')
exp2  = []
palin = False;

def palindromic(num):
    rev  = 0;
    temp = num;

    while temp > 0:
        dig  = temp %10;
        rev  = 10*rev+dig;
        temp = temp // 10;

    if(rev == num):
        return True;
    else:
        return False;

for i in exp:
    exp2.append(not(int(i) > 0)); 
    palin = palin or palindromic(int(i))

print(not any(exp2) and palin)
