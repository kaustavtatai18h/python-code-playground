import numpy;

n,m = list(map(int,input().split()))

arr = numpy.array([input().split() for _ in range(n)],dtype=int)
 
print(numpy.mean(arr,axis=1))
print(numpy.var(arr,axis=0))
print(numpy.std(arr))