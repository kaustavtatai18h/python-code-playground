import numpy

n,m = list(map(int,input().split()))

arr1 = numpy.array([input().split() for _ in range(n)],dtype=int)
arr2 = numpy.array([input().split() for _ in range(n)],dtype=int)


print(numpy.add(arr1,arr2))
print(numpy.subtract(arr1,arr2))
print(numpy.multiply(arr1,arr2))
print(numpy.array(numpy.divide(arr1,arr2),dtype=int))
print(numpy.mod(arr1,arr2))
print(numpy.power(arr1,arr2))
