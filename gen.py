#python 3.5.2

class Solution:
    
    def biggestTable(self, grid,row,col):
        rowMaxAr = 0
        rI = 0
        rJ = 0
        for i in range(row):
            line = grid[i]
            latest = self.longestSequence(line,row)

            if (latest[0] == rI) and (latest[1] == rJ):
                rowMaxAr += latest[2]
            else:
                rI = latest[0]
                rJ = latest[1]
                rowMaxAr = latest[2]   
            
        return rowMaxAr

def longestSequence(self,grid,maxLen):
    i=0
    j=0
    length = 0

    while (i<maxLen) and (j <=maxLen):
        if grid[j]:
            if i==j:
                j +=1
                length +=1
            # elif:
            #     j+=1
            #     i=j
        else:
            j+=1
            i=j        
    return [i,j,length];                  


def get_matrix():
    row = int(input())
    col = int(input())
    grid = [["0"]*col]*row

    for i in range(row):
        line = input()
        grid[i] = list(line)[0:col]
    return grid,row,col

        
if __name__ == "__main__":
    sol = Solution()
    grid,row,col = get_matrix()
    bigTable = sol.biggestTable(grid,row,col)
    print(bigTable)
