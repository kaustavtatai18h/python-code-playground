import math
t = int(input())

def sumOfHexDigits(heX):
    sum = 0
    hexAlp = {"a":10,"b":11,"c":12,"d":13,"e":14,"f":15}
    digit = 0
    
    for ch in heX:
        if ch == 'x':
            sum  +=  digit
            digit = 0
        elif ch in hexAlp :
            digit = hexAlp[ch]
        else:
            digit = int(ch)      
    return sum         


def solve():
    l, r = list(map(int,input().split(" ")))
    count = 0

    for i in range(l,r+1):
        hexSum = sumOfHexDigits(hex(i))
        if math.gcd(i,hexSum)>1:
            count += 1
    print(count)        


while t:
    solve()
    t -= 1